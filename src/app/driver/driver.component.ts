import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { URL, currentUser, getHeaderConfig } from '../../utils/constants';

@Component({
  selector: 'app-driver',
  templateUrl: './driver.component.html',
  styleUrls: ['./driver.component.scss']
})
export class DriverComponent implements OnInit {

  form!: FormGroup;
  currentUser: any = [];
  role: any = [];
  drivers: any = [];
  serverError!: String;
  isLoading!: boolean;
  private formSubmitAttempt!: boolean;

  constructor( private formBuilder: FormBuilder, private http: HttpClient, private router: Router ) { }

  ngOnInit(): void {
    console.log("currentUser", JSON.parse(currentUser));
    console.log("localStorage.getItem('currentUser')", localStorage.getItem('currentUser'));
    this.getDrivers();
    if (localStorage.getItem('currentUser') != null) {
      this.currentUser = JSON.parse(localStorage.getItem('currentUser') || '{}');
      this.role = this.currentUser?.roles[0]?.name;
      console.log("role", this.role);
      
    }
    else
      this.router.navigate(['']); 

    this.form = this.formBuilder.group({
      name: [null, [Validators.required]], 
      email: [null, Validators.required], 
      address: [null],
      phone: [null, Validators.required],
    });

  }

  getDrivers = () => {
    const path = `${URL}/driver-service`;
    this.http.get(path, { headers: getHeaderConfig() }).subscribe({
      next: data => {
        console.log("data", data);
        
        this.drivers = data;
      }
    }); 
  }

  logout = () => {
    localStorage.removeItem('token');
    localStorage.removeItem('currentUser');
    this.router.navigate(['']);
  }

  isFieldValid = (field: string) => {
    return (!this.form.get(field)!.valid && this.form.get(field)!.touched) ||
      (this.form.get(field)!.untouched && this.formSubmitAttempt);
  }
  
  validateAllFormFields = (formGroup: FormGroup) => {         
    Object.keys(formGroup.controls).forEach(field => {  
      const control = formGroup.get(field);             
      if (control instanceof FormControl) {             
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {        
        this.validateAllFormFields(control);            
      }
    });
  }
  
  setServerError = () => {
    this.serverError = ''; 
  }

  onSubmitForm = () => {
    console.log("LOG");
    
    this.serverError = '';
    this.isLoading = true;
    if (this.form.valid) {
      const { name, address, phone, email } = this.form.value;
      const body = { name, address, phone, email };
      this.http.post<any>(`${URL}/driver-service/create`, body).subscribe({
        next: data => {
          console.log("data", data);
          this.getDrivers();
          var element = document.getElementById("closeModalAdd");
          element!.click();
          this.isLoading = false;
        },
        error: err => {
          this.isLoading = false;
          console.log("err", err);
          
          if (err.status == 400)
            this.serverError = err.error.message;
          else 
            this.serverError = 'Problème de connexion au serveur';
        }
      });
    } 
    else {
      this.serverError = 'Veuillez remplir tous les champs!';
      this.isLoading = false;
      this.validateAllFormFields(this.form); 
    }
  }
  

}
