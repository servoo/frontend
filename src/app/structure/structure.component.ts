import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { URL, currentUser, getHeaderConfig } from '../../utils/constants';

@Component({
  selector: 'app-structure',
  templateUrl: './structure.component.html',
  styleUrls: ['./structure.component.scss']
})
export class StructureComponent implements OnInit {

  currentUser: any = [];
  role: any = [];
  structures: any = [];
  serverError!: String;
  isLoading!: boolean;
  private formSubmitAttempt!: boolean;

  constructor( private formBuilder: FormBuilder, private http: HttpClient, private router: Router ) { }

  ngOnInit(): void {
    console.log("currentUser", JSON.parse(currentUser));
    console.log("localStorage.getItem('currentUser')", localStorage.getItem('currentUser'));
    this.getStructures();
    if (localStorage.getItem('currentUser') != null) {
      this.currentUser = JSON.parse(localStorage.getItem('currentUser') || '{}');
      this.role = this.currentUser?.roles[0]?.name;
      console.log("role", this.role);
    }

  }

  getStructures = () => {
    const currentUser = JSON.parse(localStorage.getItem('currentUser') || '{}');
    const path = `${URL}/structure-service`;
    this.http.get(path, { headers: getHeaderConfig() }).subscribe({
      next: data => {
        console.log("data", data);
        if (currentUser?.structure?.id) {
          if(Array.isArray(data)){
            this.structures = data.filter(d => {
              if (d.id == currentUser?.structure?.id)
                return d;
            });
          }
        }
        else
          this.structures = data;
      }
    }); 
  }

  activateOrDeactivate = (id) => {
    console.log("id", id);
    const path = `${URL}/structure-service/block-or-unblock/${id}`;
    this.http.put(path, { headers: getHeaderConfig() }).subscribe({
      next: data => {
        console.log("data", data);
        this.getStructures();
      }
    }); 
  }

  logout = () => {
    localStorage.removeItem('token');
    localStorage.removeItem('currentUser');
    this.router.navigate(['']);
  }

}
