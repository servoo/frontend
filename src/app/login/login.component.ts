import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { URL, currentUser } from '../../utils/constants';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  form!: FormGroup;
  serverError!: String;
  wrongPassword!: String;
  isLoading!: boolean;
  private formSubmitAttempt!: boolean;
  currentUser: any = [];

  constructor(private formBuilder: FormBuilder, private http: HttpClient, private router: Router) { }

  ngOnInit(): void {
    // this.isLoading = false;
    // if (currentUser.length < 1 && currentUser != null)
    //   this.router.navigate(['drivers']);

    this.form = this.formBuilder.group({
      phoneOrEmail: [null, [Validators.required]], 
      password: [null, Validators.required]
    });
  }

  isFieldValid = (field: string) => {
    return (!this.form.get(field)!.valid && this.form.get(field)!.touched) ||
      (this.form.get(field)!.untouched && this.formSubmitAttempt);
  }
  
  validateAllFormFields = (formGroup: FormGroup) => {         
    Object.keys(formGroup.controls).forEach(field => {  
      const control = formGroup.get(field);             
      if (control instanceof FormControl) {             
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {        
        this.validateAllFormFields(control);            
      }
    });
  }
  
  setServerError = () => {
    this.serverError = '';
    this.wrongPassword = '';  
  }

  onSubmitForm = () => {
    this.serverError = '';
    this.wrongPassword = '';
    this.isLoading = true;
    if (this.form.valid) {
      const { phoneOrEmail, password } = this.form.value;
      const body = { phoneOrEmail, password };
      this.http.post<any>(`${URL}/auth-service/login`, body).subscribe({
        next: data => {
          this.isLoading = false
          localStorage.setItem('token', data.token);
          localStorage.setItem('currentUser', JSON.stringify(data.data));
          // this.dataSharingService.isAuthenticated.next(true); // emit login event to DataSharingService
          // this.dataSharingService.currentUser.next(JSON.stringify(data.data)) // emit login event to DataSharingService
          this.router.navigate(['/drivers']);
        },
        error: err => {
          this.isLoading = false;
          console.log("err", err);
          
          if (err.error.message == 'Mot de passe eronné' && err.status == 400) 
            this.wrongPassword = err.error.message;
          else if (err.status == 400)
            this.serverError = err.error.message;
          else 
            this.serverError = 'Problème de connexion au serveur';
        }
      });
    } 
    else {
      this.isLoading = false;
      this.validateAllFormFields(this.form); 
    }
  }

}
