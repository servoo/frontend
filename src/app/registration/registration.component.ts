import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { URL, currentUser } from '../../utils/constants';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.scss']
})
export class RegistrationComponent implements OnInit {


  formStructure!: FormGroup;
  formUser!: FormGroup;
  serverError!: String;
  idStructure!: number;
  isLoading!: boolean;
  private formSubmitAttempt!: boolean;
  currentUser: any = [];

  constructor(private formBuilder: FormBuilder, private http: HttpClient, private router: Router) { }

  ngOnInit(): void {
    this.formStructure = this.formBuilder.group({
      name: [null, [Validators.required]],
      address: [null, Validators.required],
      email: [null, Validators.required],
      phone: [null, Validators.required],
      fax: [null],
      niu: [null, Validators.required],
    });
    this.formUser = this.formBuilder.group({
      name: [null, [Validators.required]],
      address: [null],
      email: [null],
      phone: [null, Validators.required],
      phone1: [null],
      password: [null, Validators.required],
      fax: [null],
      roleName: ["ROLE_ADMIN"],
    });
  }

  isFieldValid = (field: string) => {
    return (!this.formStructure.get(field)!.valid && this.formStructure.get(field)!.touched)  ||
      (this.formStructure.get(field)!.untouched && this.formSubmitAttempt);
  }

  isFieldValidUser = (field: string) => {
    return (!this.formUser.get(field)!.valid && this.formUser.get(field)!.touched)  ||
      (this.formUser.get(field)!.untouched && this.formSubmitAttempt);
  }

  setServerError = () => {
    this.serverError = ''; 
  }
  
  validateAllFormFields = (formGroup: FormGroup) => {         
    Object.keys(formGroup.controls).forEach(field => {  
      const control = formGroup.get(field);             
      if (control instanceof FormControl) {             
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {        
        this.validateAllFormFields(control);            
      }
    });
  }

  onSubmit = async () => {
    console.log("submit !");
    var idStructure = 0
    this.serverError = '';
    this.isLoading = true;

    if (this.formStructure.valid && this.formUser.valid) {
      console.log("LOG");
      
      const { name, address, email, phone, niu, fax } = this.formStructure.value;
      const body = { name, address, email, phone, niu, fax };
      await this.http.post<any>(`${URL}/structure-service/create`, body).subscribe({
        next: data => {
          console.log("data", data);
          console.log("data.data", data.data);
          console.log("data.data?.id", data.data?.id);
          idStructure = data.data?.id;
          
          const { name, email, phone, phone1, fax, password, roleName } = this.formUser.value;
          const body2 = { name, email, phone, phone1, fax, password, roleName, idStructure: idStructure };

          this.http.post<any>(`${URL}/auth-service/register`, body2).subscribe({
            next: data => {
              console.log("data", data);
              console.log("data.data", data.data);
              this.router.navigate(['']);
              // this.isLoading = false
              // localStorage.setItem('token', data.token);
              // localStorage.setItem('currentUser', JSON.stringify(data.data));
              // this.router.navigate(['drivers']);
            },
            error: err => {
              this.isLoading = false;
              console.log("err", err);
              console.log("err.message", err.message);
              // console.log("err.data.message", err.data.message);
              
              if (err.status == 400)
                this.serverError = err.error.message;
              else 
                this.serverError = 'Problème de connexion au serveur';
            }
          }) ;
        },
        error: err => {
          this.isLoading = false;
          console.log("err", err);
          
           if (err.status == 400)
            this.serverError = err.error.message;
          else 
            this.serverError = 'Problème de connexion au serveur';
        }
      });
    } 
    else {
      // this.isLoading = false;
      this.serverError = 'Problème de connexion au serveur';
      this.validateAllFormFields(this.formStructure); 
      this.validateAllFormFields(this.formUser); 
    }
    
  }


}
