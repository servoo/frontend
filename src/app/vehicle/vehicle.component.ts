import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { URL, currentUser, getHeaderConfig } from '../../utils/constants';

@Component({
  selector: 'app-vehicle',
  templateUrl: './vehicle.component.html',
  styleUrls: ['./vehicle.component.scss']
})
export class VehicleComponent implements OnInit {

  form!: FormGroup;
  currentUser: any = [];
  role: any = [];
  vehicles: any = [];
  drivers: any = [];
  serverError!: String;
  isLoading!: boolean;
  private formSubmitAttempt!: boolean;

  constructor( private formBuilder: FormBuilder, private http: HttpClient, private router: Router ) { }

  ngOnInit(): void {
    console.log("currentUser", JSON.parse(currentUser));
    console.log("localStorage.getItem('currentUser')", localStorage.getItem('currentUser'));
    this.getVehicles();
    this.getDrivers();

    if (localStorage.getItem('currentUser') != null) {
      this.currentUser = JSON.parse(localStorage.getItem('currentUser') || '{}');
      this.role = this.currentUser?.roles[0]?.name;
      console.log("role", this.role);
      
    }
    else
      this.router.navigate(['']); 

    this.form = this.formBuilder.group({
      registrationNumber: [null, [Validators.required]], 
      capacityWeight: [null, Validators.required], 
      emptyWeight: [null],
      licenceRef: [null, Validators.required],
      description: [null],
      totalWeight: [null],
      idDriver: [null]
    });

  }

  getVehicles = () => {
    var path = "";
    const currentUser = JSON.parse(localStorage.getItem('currentUser') || '{}');
    // console.log("this.currentUser.structure?.id", currentUser);
    // console.log("this.currentUser.structure?.id", currentUser.structure?.id);
    
    if (currentUser.structure?.id) 
      path = `${URL}/vehicle-service/find-by-structure/${currentUser.structure?.id}`;
    else
      path = `${URL}/vehicle-service`;

    this.http.get(path, { headers: getHeaderConfig() }).subscribe({
      next: data => {
        console.log("data", data);
        
        this.vehicles = data;
      }
    }); 
  }
  

  getDrivers = () => {
    const path = `${URL}/driver-service`;
    this.http.get(path, { headers: getHeaderConfig() }).subscribe({
      next: data => {
        console.log("data", data);
        
        this.drivers = data;
      }
    }); 
  }

  logout = () => {
    localStorage.removeItem('token');
    localStorage.removeItem('currentUser');
    this.router.navigate(['']);
  }

  isFieldValid = (field: string) => {
    return (!this.form.get(field)!.valid && this.form.get(field)!.touched) ||
      (this.form.get(field)!.untouched && this.formSubmitAttempt);
  }
  
  validateAllFormFields = (formGroup: FormGroup) => {         
    Object.keys(formGroup.controls).forEach(field => {  
      const control = formGroup.get(field);             
      if (control instanceof FormControl) {             
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {        
        this.validateAllFormFields(control);            
      }
    });
  }
  
  setServerError = () => {
    this.serverError = ''; 
  }

  
  activateOrDeactivate = (id) => {
    console.log("id", id);
    const path = `${URL}/vehicle-service/block-or-unblock/${id}`;
    this.http.put(path, { headers: getHeaderConfig() }).subscribe({
      next: data => {
        console.log("data", data);
        this.getVehicles();
      }
    }); 
  }

  onSubmitForm = () => {
    console.log("LOG");
    console.log("this.form.value", this.form.value.idDriver);
    
    this.serverError = '';
    this.isLoading = true;
    const currentUser = JSON.parse(localStorage.getItem('currentUser') || '{}');
    if (this.form.valid) {
      const { registrationNumber, capacityWeight, emptyWeight, licenceRef, description, totalWeight, idDriver } = this.form.value;
      const body = { registrationNumber, capacityWeight, emptyWeight, licenceRef, description, totalWeight,
        driver: { id: idDriver }, structure: { id: currentUser?.structure.id } };
      this.http.post<any>(`${URL}/vehicle-service/create`, body).subscribe({
        next: data => {
          console.log("data", data);
          this.getVehicles();
          var element = document.getElementById("closeModalAdd");
          element!.click();
          this.isLoading = false;
        },
        error: err => {
          this.isLoading = false;
          console.log("err", err);
          
          if (err.status == 400)
            this.serverError = err.error.message;
          else 
            this.serverError = 'Problème de connexion au serveur';
        }
      });
    } 
    else {
      this.serverError = 'Veuillez remplir tous les champs!';
      this.isLoading = false;
      this.validateAllFormFields(this.form); 
    }
  }

}
