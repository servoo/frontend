import { CUSTOM_ELEMENTS_SCHEMA, NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule, Routes } from '@angular/router';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { FieldErrorDisplayComponentComponent } from './field-error-display-component/field-error-display-component.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { RegistrationComponent } from './registration/registration.component';
import { DriverComponent } from './driver/driver.component';
import { DataSharingService } from './data-sharing.service';
import { SidebarComponent } from './sidebar/sidebar.component';
import { StructureComponent } from './structure/structure.component';
import { VehicleComponent } from './vehicle/vehicle.component';

const appRoutes: Routes = [
  { path: '', component: LoginComponent },
  { path: 'register', component: RegistrationComponent },
  { path: 'drivers', component: DriverComponent },
  { path: 'structures', component: StructureComponent },
  { path: 'vehicles', component: VehicleComponent },
]
@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    FieldErrorDisplayComponentComponent,
    RegistrationComponent,
    DriverComponent,
    SidebarComponent,
    StructureComponent,
    VehicleComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule, 
    RouterModule.forRoot(appRoutes),
    FormsModule, ReactiveFormsModule,
  ],
  providers: [DataSharingService],
  bootstrap: [AppComponent],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA ]

})
export class AppModule { }
