export const URL = "http://localhost:9191";
export const token = localStorage.getItem('token');
export const currentUser = localStorage.getItem('currentUser');
export const isAuthenticated = localStorage.getItem('isAuthenticated');

export const getHeaderConfig = () =>{
    return{
        headers:{
            // Authorization: localStorage.getItem('token'),
            ContentType: 'application/json',
        }
    }
}

// for multipart request if needed
// export const getFormDataHeaderConfig = () =>{
//     return{
//         headers:{
//             Authorization: localStorage.getItem('token'),
//             ContentType: 'multipart/form-data',
//             Accept: 'application/json'
//         }
//     }
// }